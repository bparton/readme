# 🔮 README

## Brianne's README

**Brianne Parton - Customer Success Manager, Strategic AMER East**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## About me

- Live and work out of Los Angeles, CA
- Domesticated, six years in August 2023 😁
- 2 cats, 2 dogs (v large puppies at 65 and 75 pounds of pure love)
- Distant, and I mean distant, relative of Dolly Parton
- Horror and true crime are my typical movie/tv genres, but also love rom-coms (I've seen Pretty Woman not enough times)
- Drawn to vulnerability, ownership, and transparency

## How you can help me

- Appreciate direct and honest feedback with constructive ideas on how to improve - no need for sugar coating or sandwiching (I'll ask what I'm doing correct if I feel I need clarity)
- I have ADHD, I need reminders, seriously - tell me again because I'd appreciate that
- Please don't apologize for anything out of your control, this is a "no sorry" zone; I feel this is different than accountability

## My working style

- I learn best by doing and making mistakes
- Personality: [Mediator, INFP-A](https://www.16personalities.com/infp-personality) (this means I can easily talk myself out of going to social events)
- I try my best to practice deep work in the mornings, no interruptions 
- When scheduling meetings, I prefer to have an agenda and understanding of what is expected of me prior to joining (customer calls and internal)
- If I ask to chat via slack, I'll do my best to give a quick explanation as to what it is about to prevent anxiety and definitely appreciate this direct communication from my colleagues as well. 

## What I assume about others

- I assume positive intent with everyone
- If I ping you a second time, it's only to follow up and for no other reason; I understand we're all busy and working remotely and async
- I do asssume that if there's an issue involving myself that I will be informed immediately so I can attempt to remedy whatever it may be
- I assume that if an issue or escalation arises, we will collective take accountability and not place blame as we're all a team 

## What I want to earn

- I want to earn great working relationships with everyone! 

## Communicating with me

- I prefer slack 99% of the time
- I'm a big believer in work/life balance and as such I'm strict with my working hours
- My calendar will be up-to-date with personal appointments etc and working hours
- I do not have slack on my phone 🤪

## Strengths/Weaknesses

- A big strength is accountability and direct communication (of course, while being empathetic and vulnerable myself); I'm okay with constructive confrontation 
- Biggest weakness is it is really easy for me to self-isolate and not ask for help (actively working through this in therapy 🥲)

